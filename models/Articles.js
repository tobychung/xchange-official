var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var ArticlesSchema = new Schema({
    id: {
        type: Number
    },
    title: {
        type: String
    },
    genre: {
        type: Number
    },
    source: {
        type: String
    },
    praise_count: {
        type: Number
    },
    comment_count: {
        type: Number
    },
    publish_time: {
        type: Date
    },
    banner_pic: {
        type: String
    }
}, {
    collection: "teams"
});

module.exports = articles = mongoose.model('Articles', ArticlesSchema);