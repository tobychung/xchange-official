//----------------  Variable  ----------------------//
const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3009 : process.env.PORT;
const app = express();

//--------------------------
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
// all environments

app.set('port', process.env.PORT || 3000);
// app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(methodOverride());


//頁籤logo
// app.use(favicon(__dirname + '/app/base/favicon.ico'));

//----------------  DB ----------------------//
app.set('env', 'development_local');
require('./db')(app);


//-------------------  開發 vs 未開發  -------------------------//

if (isDeveloping) {

    console.log('dev');
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: 'errors-only'

        // {
        //     colors: true,
        //     hash: false,
        //     timings: true,
        //     chunks: false,
        //     chunkModules: false,
        //     modules: false
        // }
    });



    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));


    app.get(['/'], function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.end();
    });



} else {


    console.log('release');
    app.use(express.static(__dirname + '/dist'));
    app.get('/', function response(req, res) {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
}



app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info('==> 🌎 Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port);
});