//----------------  Variable  ----------------------//
const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3003 : process.env.PORT;
const app = express();

var favicon = require('serve-favicon');

//--------------------------
var React = require('react');
var Router = require('react-router');
// var routes = require('./routes');
var http = require('http');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');



var session = require('express-session');
// var MongoStore = require('connect-mongo')(session);
var logger = require('express-logger');
var errorHandler = require('errorhandler');
// var engine = require('ejs-locals');

// all environments

app.set('port', process.env.PORT || 3003);
// app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(methodOverride());

//----------------  想辦法route  ----------------------//

// app.use(function(req, res, next) {
//     var router = Router.create({
//         location: req.url,
//         routes: routes
//     })
//     router.run(function(Handler, state) {
//         var html = React.renderToString(<Handler/>)
//         return res.render('react_page', {
//             html: html
//         })
//     })
// })
//----------------  轉換xls  ----------------------//

var node_xj = require("xls-to-json");
node_xj({
    input: "./xls/xchange_db.xls",  // input xls 
    output: "./json/xchange_db.json", // output json 
    sheet: "Index"  // specific sheetname 
}, function (err, result) {
    if (err) {
        console.error(err);
    } else {
        console.log(result);
    }
});



//----------------  call Controllers  ----------------------//
// var controllers = require('./controllers');

// app.get('/', controllers.index);


//頁籤logo
// app.use(favicon(__dirname + '/app/base/favicon.ico'));

// //----------------  DB ----------------------//
// app.set('env', 'development_local');
// require('./db')(app);

// console.log('wee', wee);
// app.use(session({
//     secret: "mongo",
//     store: new MongoStore({
//         host: 'localhost',
//         port: 27017,
//         db: 'MongoDB',
//         collection: "sessions",
//         url: 'mongodb://localhost:27017/teams'
//     }),
//     saveUninitialized: true,
//     resave: true,
// }));

// app.use(errorHandler({
//     dumpExceptions: true,
//     showStack: true
// }));

// create a write stream (in append mode)
// var accessLogStream = fs.createWriteStream(__dirname + '/log/access.log', {flags: 'a'})

// setup the logger
// app.use(morgan('combined', {stream: accessLogStream}))
// app.use(logger({
//     path: __dirname + '/log/request_development.log'
// }));



//----------------  Socket.io ----------------------//

var server = require('http').createServer(app);
var io = require('socket.io')(server);
var onlineUsers = 0;

io.sockets.on('connection', function(socket) {
    onlineUsers++;
    console.log('onlineUsers', onlineUsers);
    io.sockets.emit('onlineUsers', {
        onlineUsers: onlineUsers
    });

    socket.on('disconnect', function() {
        onlineUsers--;
        io.sockets.emit('onlineUsers', {
            onlineUsers: onlineUsers
        });
    });
});



//-------------------  開發 vs 未開發  -------------------------//

if (isDeveloping) {

    console.log('dev');
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: 'errors-only'

        // {
        //     colors: true,
        //     hash: false,
        //     timings: true,
        //     chunks: false,
        //     chunkModules: false,
        //     modules: false
        // }
    });



    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));


    app.get('*', function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.end();
    });



} else {


    console.log('release');
    app.use(express.static(__dirname + '/dist'));
    app.get('/', function response(req, res) {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
}



// var mongoose = require('mongoose');

// var router = app.Router();

//列表页get数据的请求地址



var router = express.Router();



// var fetch = require('node-fetch');
app.get('/articles', function(req, res, next) {

    var User = require('./models/user.js');

    User.find({}, function(err, results) {
        if (err) {
            console.log('error message', err);
            return;
        } else {
            console.log('請求成功');
            res.json(results);
        }

    })


});



app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info('==> 🌎 Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port);
});