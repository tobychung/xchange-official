import Immutable from "immutable";


import faviconUrl from '../images/global/favicon.ico';




//---------- Img ------------//

import bannerImg from "../assets/images/index/index-banner.jpg";
import coopImg from "../assets/images/index/index-coop-icons.png";
//------------------

import indexEvent01 from "../assets/images/index/events/events_01.jpg";
import indexEvent02 from "../assets/images/index/events/events_02.jpg";
import indexEvent03 from "../assets/images/index/events/events_03.jpg";
import indexEvent04 from "../assets/images/index/events/events_04.jpg";











import intro01 from "../assets/images/index/index-intro-img-01.jpg";
import intro02 from "../assets/images/index/index-intro-img-02.jpg";
import intro03 from "../assets/images/index/index-intro-img-03.jpg";

//------------------

import avatar01 from "../assets/images/index/index-avatar-01.png";
import avatar02 from "../assets/images/index/index-avatar-02.png";
import avatar03 from "../assets/images/index/index-avatar-03.png";
import avatar04 from "../assets/images/index/index-avatar-04.png";
import avatar05 from "../assets/images/index/index-avatar-05.png";
import avatar06 from "../assets/images/index/index-avatar-06.png";
import avatar07 from "../assets/images/index/index-avatar-07.png";
import avatar08 from "../assets/images/index/index-avatar-08.png";
import avatar09 from "../assets/images/index/index-avatar-09.png";
import avatar10 from "../assets/images/index/index-avatar-10.png";
import avatar11 from "../assets/images/index/index-avatar-11.png";
import avatar12 from "../assets/images/index/index-avatar-12.png";
import avatar13 from "../assets/images/index/index-avatar-13.png";
import avatar14 from "../assets/images/index/index-avatar-14.png";
import avatar15 from "../assets/images/index/index-avatar-15.png";
import avatar16 from "../assets/images/index/index-avatar-16.png";



// import eventAvatar01 from "../assets/images/event/event-avatar-01.png";
// import eventAvatar02 from "../assets/images/event/event-avatar-02.png";


import eventCarousel01 from "../assets/images/event/event-carousel-01.jpg";
import eventCarousel02 from "../assets/images/event/event-carousel-02.jpg";
import eventCarousel03 from "../assets/images/event/event-carousel-03.jpg";




//---------- Json ------------//

// import indexJson from "../../json/xchange_db.json";

//==================================================//

export const initialBasicState = {


    metaObj: {
        title: 'XChange Official',
        description: 'XChange Official',
        link: {
            rel: {
                icon: faviconUrl
            }
        },
        meta: {
            charset: 'utf-8',
            name: {
                viewport: 'initial-scale=1.0, width=device-width'
            }
        }

    },


    naviArr: [{
        "text": "WHO WE ARE",
        "url": "/"
    }, {
        "text": "EVENT",
        "url": "/event"
    }, {
        "text": "JOIN US",
        "url": "/join"
    }],



    main: {

        bannerImg:bannerImg,
        coopImg:coopImg,

        thumbnailsArr: [{

            title: "產業論壇",
            info: "這裡聚集全台最強電商, 新創, 數位廣告及網路同業的年輕人",
            imgUrl: indexEvent01
        }, {

            title: '經營團隊限定小聚',
            info: "每月下旬定期舉辦限額100人內講座. 邀請兩位業界前輩分享know-how及實戰經驗",
            imgUrl: indexEvent02
        }, {

            title: '海外工作同學會',
            info: "不定期舉辦休閒聚會, 一起學手沖咖啡, 一起野餐, 品酒, 聊音樂. 豐富下班後的生活",
            imgUrl: indexEvent03
        }, {

            title: '活動',
            info: "不定期舉辦休閒聚會, 一起學手沖咖啡, 一起野餐, 品酒, 聊音樂. 豐富下班後的生活",
            imgUrl: indexEvent04
        }],

        membersArr: [{"img":"01","id":"1","name":"黃獻德(TonTon)","company":"雪豹科技","pos":"深度學習專家"}]

    },


    event: {

        eventBlockArr:[

            {
                title:"Cheetah  X  Dcard",
                info:"",
                lecturer:["",""],
                jobTilte:["",""],
                avatarArr:[],
                noteLink:["","",""],
                
            },
            {
                title:"Cheetah  X  Dcard",
                info:"",
                lecturer:["",""],
                jobTilte:["",""],
                avatarArr:[],
                noteLink:["","",""],
                
            },
            {
                title:"Cheetah  X  Dcard",
                info:"",
                lecturer:["",""],
                jobTilte:["",""],
                avatarArr:[],
                noteLink:["","",""],
                
            }

        ]


    },

    style:{
        header:{ boxShadow: "none"},
        headerLink:{}
    }


};