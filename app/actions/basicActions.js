require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch'
import axios from 'axios'


import {
    createActions
} from 'redux-actions';

import store from '../store';

export const {

    setAllData,
    toggleHeaderStyle

} = createActions({

    SET_ALL_DATA: (obj) => obj,
    TOGGLE_HEADER_STYLE:(obj) => obj,


});