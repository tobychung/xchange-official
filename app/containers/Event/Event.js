//-------  CSS  --------//
import './event.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    browserHistory
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'


//---------- Store ------------//
import store from '../../store';

import {toggleHeaderStyle} from '../../actions/basicActions';

//---------- Vendor ------------//
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

import Slider from 'react-slick';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import EventList from "../../components/EventList/EventList.js";

//---------- Json ------------//

import discuzData from "../../../public/json/event/Event-discuz-Section-01.json";
import overseaData from "../../../public/json/event/Event-oversea-Section-01.json";
import meetupData from "../../../public/json/event/Event-meetup-Section-01.json";


const partArr = [
    {
        title: "discuz",
        titleCH: "產業論壇"
    },
    {
        title: "oversea",
        titleCH: "海外工作同學會"
    },
    {
        title: "meetup",
        titleCH: "團隊小聚"
    }
];

//==================================================//

class Event extends Component {



    constructor(props) {

        super(props);
        this.state = { 
            tabIndex: 0,
            currPart: {
                title: null,
                titleCH: "近期活動"
            }
        };
    }

    componentWillMount(){
        store.dispatch(toggleHeaderStyle(true));
    }



    componentDidMount(){
        window.scrollTo(0, 0);
    }



    render() {

        // var settings = {
        //     dots: true,
        //     infinite: false,
        //     speed: 500,
        //     slidesToShow: 1,
        //     slidesToScroll: 1
        // };


        const {
            currPart
        } = this.state;
        
        return (    


            <div className="event-bg index-content global-content">
                
                <div className="banner-bg">
                    <img className="event-banner" src={require(`../../../public/img/event/${currPart.title || "discuz" }/banner-img.png`)} />
                    <span>{currPart.title && currPart.title.toUpperCase() || "EVENT" } | {currPart.titleCH}</span>
                </div>
                    
                <div className="slider-container">

                    {/* <Slider {...settings}>
                        <div className="index-li">
                            <img src={eventSlideImg01}/>
                        </div>
                        <div className="index-li">
                            <img src={eventSlideImg02} />
                        </div>
                        <div className="index-li">
                            <img src={eventSlideImg03} />
                        </div>
                    </Slider> */}

                </div>

                <div className="tabs-container">
                    <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => {
                        
                        this.setState({ 
                            tabIndex: tabIndex,
                            currPart: partArr[tabIndex] 
                        });

                    }}>
                        <TabList>
                            <Tab>產業論壇</Tab>
                            <div className="divide-line"></div>
                            <Tab>海外工作同學會</Tab>
                            <div className="divide-line"></div>
                            <Tab>團隊小聚</Tab>
                        </TabList>

                        <TabPanel>

                            <div className="tabs-01">
                                <EventList arr={discuzData.detail} part="discuz"/>
                            </div>
                            
                        </TabPanel>
                        <TabPanel>
                            <div className="tabs-02">
                                <EventList arr={overseaData.detail} part="oversea"/>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="tabs-03">
                                <EventList arr={meetupData.detail} part="meetup"/>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return state;
};


export default connect(
    mapStateToProps
)(Event);