//-------  CSS  --------//
import './join.min.css';
//-------  Basic  --------//
import React, {
    Component
} from 'react';

//-------  Redux  --------//
import {
    connect
} from 'react-redux';



//---------- Vendor ------------//
import ScrollAnim from 'rc-scroll-anim';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import Animate from 'rc-animate';
const ScrollOverPack = ScrollAnim.OverPack;

//---------- Components ------------//
import store from "../../store";
import {toggleHeaderStyle} from '../../actions/basicActions';

//---------- Img ------------//
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const partArr = [
    {
        title:"partner",
        titleCH:"合作夥伴"
    },
    {
        title:"members",
        titleCH:"團隊成員"
    }
];

//==================================================//

class About extends Component {



    constructor(props) {

        super(props);
        this.state = {
            partIndex: 0,
            currPart: {
                title:"partner",
                titleCH:"合作夥伴"
            }
        };
    }
    
    onWaypointEnter(){
        store.dispatch(toggleHeaderStyle(false));
    }
    onWaypointLeave(){
        store.dispatch(toggleHeaderStyle(true));
    }
    componentDidMount(){
        window.scrollTo(0, 0);
    }

    render() {

        const {
            currPart
        } = this.state;

        return (
            <div className="join-bg index-content global-content">
                
                <div className="banner-bg">
                    <img className="event-banner" src={require(`../../../public/img/join/${currPart.title}/banner-img.png`)} />
                    <span>{currPart.title.toUpperCase()} | {currPart.titleCH}</span>
                </div>
                

                <div className="tabs-container">
                    <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => {
                        
                        this.setState({ 
                            tabIndex: tabIndex,
                            currPart: partArr[tabIndex].title  
                        });

                    }
                    }>
                        <TabList>
                            <Tab>產業論壇</Tab>
                            <div className="divide-line"></div>
                            <Tab>海外工作同學會</Tab>
                            <div className="divide-line"></div>
                            <Tab>團隊小聚</Tab>
                        </TabList>

                        <TabPanel>

                            <div className="tabs-01">
                            </div>
                            
                        </TabPanel>
                        <TabPanel>
                            <div className="tabs-02">
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="tabs-03">
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>

               
                
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(
    mapStateToProps
)(About);