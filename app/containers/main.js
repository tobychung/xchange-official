//-------  CSS  --------//
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    connect
} from 'react-redux'

//---------- Actions ------------//
// import {
//     fetchNews,
//     fetchClass,
//     fetchProduct,

// } from '../actions/dbActions';

//---------- Store ------------//
import store from '../store';


//---------- Components ------------//

import HeroImage from '../components/HeroImage/HeroImage.js';

//---------- Img ------------//
import newsBlockTitle from './img/news-title.png';
//==================================================//

class Main extends Component {



    constructor(props) {

        super(props)

    }


    componentDidMount() {

        const {
            dispatch
        } = this.props;



    }


    render() {
        return (
            <div className="main-bg index-content global-content">
                
                <HeroImage/>
                
            </div>
        );
    }
};


const mapStateToProps = (state) => {
    // console.log('[main] state', state);
    return state;
}


export default connect(

    mapStateToProps,

)(Main)