//-------  CSS  --------//
import "./main.min.css";
//-------  Basic  --------//
import React, {
    Component
} from "react";

//-------  Redux  --------//
import {
    connect
} from "react-redux";
import {
    toggleHeaderStyle
} from "../../actions/basicActions";



//---------- Store ------------//
import store from "../../store";
//---------- Vendor ------------//

import Waypoint from "react-waypoint";

//---------- Components ------------//

import HeroImage from "../../components/HeroImage/HeroImage.js";
import Thumbnails from "../../components/Thumbnails/Thumbnails.js";
import MemberIntro from "../../components/MemberIntro/MemberIntro.js";

//---------- Img ------------//

import logoImg from "../../assets/images/index/250x250_logo.png";

const sectionData01 = require("../../../public/json/home/Home-Page-Section-01.json");
const sectionData02 = require("../../../public/json/home/Home-Page-Section-02.json");
const sectionData03 = require("../../../public/json/home/Home-Page-Section-03.json");

//==================================================//

class Main extends Component {



    constructor(props) {

        super(props);
        this.state = {
             style: {}
        };
    }
    onWaypointEnter(){
        store.dispatch(toggleHeaderStyle(false));
    }
    onWaypointLeave(){
        store.dispatch(toggleHeaderStyle(true));
    }

    render() {

        const {
            main
        } = this.props.basic;
        return (


            <div className="main-bg index-content global-content">
                
                    
                    <Waypoint
                        onEnter={this.onWaypointEnter}
                        onLeave={this.onWaypointLeave}
                    >
                    <div>
                        <HeroImage/>
                    </div>
                    </Waypoint>
                    <div className="section-01 section-grp">
                        <div className="section-01-info">


                            <img className="section-01-icon" src={logoImg}/>
                            {/* <img className="section-02-icon" src="http://fakeimg.pl/400x40/"/>*/}
                            <h5>"X" 代表 "你 X 我"一起合作，</h5>
                            <h5>在互相give & take之間，擦出火花、帶出改變！</h5>
                            <h5>我們聚集網路圈的年輕人，一起在專業上學習、一起在工作外用心生活</h5>
                            <h5>邀請您來聽業界前輩的分享，同時認識同產業的好朋友:)</h5>
                        </div>
                        <Thumbnails arr={main.thumbnailsArr}/>
                    </div>
                

                
                    
                    <div className="divide-img divide-img-01">Our Speaker</div>

                    <div className="section-02 section-grp">
                        <MemberIntro arr={sectionData02.detail}/>
                    </div>
                
                    <div className="divide-img divide-img-02">Our Connection</div>
                
                    <div className="section-03 section-grp">
                        <img className="coop-img" src={main.coopImg}/>
                    </div>

                
            </div>

        );
    }
};


const mapStateToProps = (state) => {
    // console.log("[main] state", state);
    return state;
};


export default connect(

    mapStateToProps,

)(Main);