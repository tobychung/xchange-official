//-------  Basic  --------//
import Immutable from 'immutable';

import {
    handleActions
} from 'redux-actions';

//-------  Initial State  --------//
import {
    initialBasicState
} from '../constants/models';

//==================================================//


export default handleActions({

    SET_ALL_DATA: (state, {
        payload
    }) => {



        const newState = Immutable.fromJS(state)
            // .set('brewItemsArr', resultArr)
            .toJS();

        return newState;

    },
    TOGGLE_HEADER_STYLE: (state, {
        payload
    }) => {

        let headerStyle = payload?{backgroundColor:"#fff"}:{boxShadow:"none"} 
        let linkStyle = payload?{color:"#000"}:{}


        const newState = Immutable.fromJS(state)
            .setIn(['style','header'], headerStyle)
            .setIn(['style','headerLink'], linkStyle)
            .toJS();

        return newState;

    },



}, initialBasicState);