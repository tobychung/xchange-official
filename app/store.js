//-------  Redux  --------//

import Immutable from 'immutable';

import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers
} from 'redux';

import {
    combineForms,
    modelReducer,
    formReducer,
    modeled

} from 'react-redux-form';



//-------  Middleware  --------//

import thunkMiddleware from 'redux-thunk';
// import loggerMiddleware from 'redux-logger';


//-------  Actions  --------//



//-------  Reducers  --------//

import {

    initialUserState

} from './constants/models';


import basic from './reducers/basicReducers';



//-------  Reducer 集合  --------//


const rootReducer = combineReducers({

    // deep: combineForms({

    //     user: initialUserState
    //         // userForm: formReducer('user', initialUserState),


    // }, 'deep'),

    user: modelReducer('user', initialUserState),
    userForm: formReducer('user', initialUserState),



    basic


});

//-------  Store  --------//

const store = createStore(

    rootReducer,

    applyMiddleware(
        thunkMiddleware,
        // loggerMiddleware
    )
)



export default store;