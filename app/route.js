//-------  Basic  --------//
import React from 'react';
import ReactDOM from 'react-dom';

//-------  Router  --------//
import {
    Router,
    Route,
    browserHistory,
    IndexRoute
} from 'react-router';
import {
    Provider
} from 'react-redux';
import store from './store';

//---------- Containers ------------//
import Index from './index.js';
import Main from './containers/Main/Main.js';
import Event from './containers/Event/Event.js';
import Join from './containers/Join/Join.js';
import Detail from "./components/Event/Detail.js";
//==================================================//

ReactDOM.render((
    <Provider store={store}>
        <Router history = {browserHistory}>
            <Route path = "/" component = {Index}>
                <IndexRoute component = {Main} />
                <Route path="/event" component={Event}/>
                <Route path="/join" component={Join}/>
                <Route path="/discuz" component={Detail}/>
                <Route path="/oversea" component={Detail}/>
                <Route path="/meetup" component={Detail}/>
            </Route>
        </Router>
    </Provider>


), document.getElementById('body'));