import './EventList.min.css';
//-------  Basic  --------//
import React, {
    Component
} from 'react';

import {
    Link
} from "react-router";


//==================================================//



class EventListItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { item,part } = this.props;
        return (

            <li className="event-list-li" onClick={this.handleClick}>
				<Link to={`/${part}?iid=${item.no}`}>
                    <img src="http://fakeimg.pl/350x200/"/>
                    
                    <h4 className="title">{item.topic}</h4>
                    <h6 className="date">{item.date}</h6>
                    <span className="info">{item.text}</span>
                </Link>
            </li>


        );
    }
}


class EventList extends Component {



    constructor(props) {
        super(props);
        this.generateItem = this.generateItem.bind(this);
        
    }

   
    generateItem(item, index) {
        return <EventListItem key={index} item={item} part={this.props.part}/>;
    }


    
    render() {

        let list = this.props.arr.map(this.generateItem);


        return (
            <ul className="event-list-comp">
				{list}
            </ul>
        );
    }
};



export default EventList;