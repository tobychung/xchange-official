//-------  CSS  --------//
import './thumbnails.min.css';


//-------  Basic  --------//
import React, {
    Component
} from 'react';

//-------  Redux  --------//
import {
    connect
} from 'react-redux';


//==================================================//



class ThumbnailsItem extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <li className="thumbnails-li">
                <img src={this.props.imgUrl}/>
               <span className="title">{this.props.title}</span>
               <h5 className="info">{this.props.info}</h5>
            </li>
        );
    }
};


class Thumbnails extends Component {

    constructor(props) {

        super(props);

    }

    generateItem(item, index) {

        return <ThumbnailsItem key={index} title={item.title} info={item.info} imgUrl={item.imgUrl} />
    }

    render() {

        // console.log('this.props', this.props);
        var items = this.props.arr.map(this.generateItem);
        return (
            <ul className="thumbnails-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(Thumbnails);