//-------  CSS  --------//
import "./memberIntro.min.css";
import React, {
    Component
} from "react";
import {
    connect
} from "react-redux";
import ScrollAnim from "rc-scroll-anim";
import QueueAnim from "rc-queue-anim";
const ScrollOverPack = ScrollAnim.OverPack;
//==================================================//



class MemberIntroItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    render() {
        return (
            <li key={this.props.key} className="member-intro-li">
                <img src={require(`../../../public/img/home/Section-02/${this.props.imgUrl}`)} />
               <h5 className="name">{this.props.name}</h5>
               <h5 className="company">{this.props.company}</h5>
               <h5 className="pos">{this.props.pos}</h5>
            </li>
        );
    }
};


class MemberIntro extends Component {

    constructor(props) {
        super(props);
    }

    generateItem(item, index) {

        return <MemberIntroItem key={index} name={item.name} company={item.company} pos={item.pos} imgUrl={item.imgUrl} />
    }

    render() {

        // console.log("[memberInfo] arr", this.props.arr);
        let items = this.props.arr.map(this.generateItem);
        return (
            <ul className="member-intro-comp">
                <ScrollOverPack hideProps={{ tweenOne: { reverse: true }}}>
                    <QueueAnim key="queueAnim" type="top">
                        {items}
                    </QueueAnim>
                </ScrollOverPack>
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(MemberIntro);