//-------  CSS  --------//
import './header.min.css';

//-------  Basic  --------//
import React, {
    Component
} from 'react';

//-------  Redux  --------//
import {
    connect
} from 'react-redux';

//---------- Components ------------//

import NavBar from '../../components/Navbar/NavBar.js';


//---------- Img ------------//
import logoUrl from '../../images/main/logo-icon.png';

//==================================================//


class Header extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {
            basic
        } = this.props;
        return (

            //全部包含ul
            <div className="header" style={basic.style.header}>     
                
                <div className="logo-container">
                    <a href="/"><img src={logoUrl} /></a>
                </div>
                
                <div className="navi-container">
                    <NavBar naviData={this.props.basic.naviArr}/>
                </div>

            </div>
        );
    }
};

const mapStoreToProps = (state) => {
    return state;
};

export default connect(
    mapStoreToProps
)(Header);