//-------  CSS  --------//
import './thumbnails.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//

//---------- Store ------------//
import store from '../../store';

//---------- Components ------------//

//---------- Img ------------//


//==================================================//



class ThumbnailsItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {



    }

    render() {

        console.log("this.props",this.props);

        return (
            <li className="thumbnails-li">
                <img src={require(`../../assets/images/index/index-avatar-${this.props.img}.png`)}/>
               <span className="title">{this.props.title}</span>
               <h5 className="info">{this.props.info}</h5>
            </li>
        );
    }
};


class Thumbnails extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {

        return <ThumbnailsItem key={index} title={item.title} info={item.info} img={item.img} />
    }

    render() {

        // console.log('this.props', this.props);
        var items = this.props.arr.map(this.generateItem);
        return (
            <ul className="thumbnails-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(Thumbnails);