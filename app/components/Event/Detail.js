import './detail.min.css';

import React, {
    Component
} from 'react';

import queryString from "query-string";

import {
    connect
} from 'react-redux';

// import template from "./detail.rt";

import _ from "lodash";
import discuzData from "../../../public/json/event/Event-discuz-Section-01.json";
import meetupData from "../../../public/json/event/Event-meetup-Section-01.json";
import overseaData from "../../../public/json/event/Event-oversea-Section-01.json";

//==================================================//

class Detail extends Component {

    constructor(props) {
        super(props);
        this.handleGetData = this.handleGetData.bind(this);
        this.state = {
            no : 0
        };
    }

    componentWillMount(){
        let query = queryString.parse(location.search);
        this.setState({
            no: parseInt(query.iid)
        });
        console.log("query",query,this.props.location.pathname);
    }

    handleGetData(page,no){
        console.log("nowPage",page,no);
        let detail = {};
        switch (page) {
            case "/discuz":
                detail = _.find(discuzData.detail,{'no': no}); 
                break;
            case "/meetup":
                detail = _.find(meetupData.detail,{'no': no}); 
                break;
            case "/oversea":
                detail = _.find(overseaData.detail,{'no':no}); 
                break;
            default:
                break;
        }
        console.log("detail",detail);
        return detail;

    }


    render() {

        let detailData = this.handleGetData(this.props.location.pathname,this.state.no);
        // return template.apply(this);
        return (
            <div className="detail-bg global-content">
                <img className="detail-banner" src={require(`../../../public/img/event${this.props.location.pathname}/banner-img-detail.jpg`)} />
                <div className="detail-con">


                    <h1>{detailData.topic}</h1>

                    <h5>時間｜Event Datetime</h5>
                    <p>{detailData.date}{detailData.time}</p>
                    <h5>地點｜Event Location</h5>

                    <h5>資訊｜Event Datetime</h5>
                    <h5>講者｜Event Guest</h5>
                    <h5>地點｜Event Datetime</h5>



                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return state;
};


export default connect(
    mapStateToProps
)(Detail);