//-------  CSS  --------//
import './footer.min.css';
//-------  Basic  --------//
import React, {

    Component,
    PropTypes

} from 'react';
//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';
//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';
//---------- Actions ------------//


//---------- Components ------------//

//---------- Img ------------//

import icon01 from './social-media-icon-01.png';
import icon02 from './social-media-icon-02.png';
import icon03 from './social-media-icon-03.png';


//==================================================//
class Footer extends Component {

    render() {
        return (

            <div className="footer">   
   

                <div className="top-part-bg">
                    
                    <div className='info-container'>

                        <div className='info-01 info-grp'>

                            <div className='info-title'>關於XChange</div>
                            
                            <div className='info-content-01'>
                                
                                <ul>
                                    <li><Link to="/news">最新消息</Link></li>
                                    <li><Link to="/brew">近期活動</Link></li>
                                    <li><Link to="/class">關於我們</Link></li>
                                    <li><Link to="/product">加入團隊</Link></li>
                                </ul>
                                
                            </div>
                        </div>

                        <div className='info-02  info-grp'>
                            <div className='info-title'>聯絡我們</div>
                            <div className='info-content-02'>
                                
                                <ul>
                                    <li>台北市信義區信義路五段7號83樓</li>
                                    <li>02 - 2222222</li>
                                    
                                </ul>
                                
                            </div>                
        
                        </div>

                        <div className='info-03  info-grp'>
                            <div className='info-title'>追蹤我們</div>
                            <div className='info-content-03'>
                                
                                <ul>
                                    <a href="https://www.facebook.com/groups/811949022260117/?fref=ts"><img src={icon01}/></a>
                                   
                                    <a href="https://www.instagram.com"><img src={icon03}/></a>
                                    
                                </ul>
                                
                            </div>

                        </div>
                        


                    </div>
                </div>

          

                <div className='bottom-part-bg'>
                    <span>Copyright © XChange. All rights reserved</span> 
                </div>
               

                  
            </div>
        );
    }

};


export default Footer;