//-------  CSS  --------//
import './heroImage.min.css';

//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//
// import {
//     emptyCart,
//     addToCart,
//     sumUp
// } from '../../actions/cartActions';

//---------- Store ------------//
import store from '../../store';

//---------- Vendor ------------//
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

//---------- Components ------------//
//---------- Img ------------//

import headerImg from "../../assets/images/index/140x140_LOGO.png";

//==================================================//



class HeroImage extends Component {


    constructor(props) {

        super(props);

    }



    render() {

        // console.log('[heroimage]this.props', this.props.data.heroimage);
        // let heroimageArr = this.props.data.heroimage;


        return (

            <div className="hero-image-bg">
                
                <ReactCSSTransitionGroup transitionName="text-fade" 
                    transitionAppear={true}
                    transitionAppearTimeout={5000} 
                    transitionEnter={false}
                    transitionLeave={false}>
                    <div className='title-container'>
                        
                        <img className="title-img" src={headerImg}/>
                        <div className="info">
                            <h5>匯聚 台灣網路圈新生代的</h5>
                            <h5>KNOW-HOW & CONNECTION</h5>
                        </div>
                        <Link to="/event"><button>VIEW OUR EVENT</button></Link>


                    </div>
              
                </ReactCSSTransitionGroup>

            </div>


        );
    }
};


const mapStateToProps = (state) => {

    return state;

}

export default connect(
    mapStateToProps
)(HeroImage);