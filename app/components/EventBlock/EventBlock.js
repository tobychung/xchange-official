//-------  CSS  --------//
import './EventBlock.min.css';


//-------  Basic  --------//
import Immutable from 'immutable';

import React, {
    Component,
    PropTypes
} from 'react';

//-------  Router  --------//
import {
    Router,
    Route,
    Link
} from 'react-router';

//-------  Redux  --------//
import {
    Provider,
    connect
} from 'react-redux';

//---------- Actions ------------//

//---------- Store ------------//
import store from '../../store';

//---------- Components ------------//

//---------- Img ------------//


//==================================================//



class EventBlockItem extends Component {

    constructor(props) {

        super(props);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick() {




    }

    render() {



        return (
            <li className="event-block-li">
               
               <div className="left-col">
                    <h1></h1>

               </div>

               <div className="right-col"></div>
               
               
               
               
               <img src={this.props.imgUrl}/>
               <span className="title">{this.props.title}</span>
               <h5 className="info">{this.props.info}</h5>
            </li>
        );
    }
};


class EventBlock extends Component {

    constructor(props) {

        super(props)

    }

    generateItem(item, index) {

        return <EventBlockItem key={index} title={item.title} info={item.info} imgUrl={item.imgUrl} />
    }

    render() {

        // console.log('this.props', this.props);

        let {event} = this.props.basic;


        var items = event.eventBlockArr.map(this.generateItem);
        return (
            <ul className="event-block-comp">
                {items}
            </ul>
        );
    }

};


const mapStateToProps = (state) => {
    return state;
}


export default connect(
    mapStateToProps
)(EventBlock);